import logging

from cryptography.hazmat.primitives.serialization import Encoding
from cryptography.x509 import ocsp

from .utils import async_retry


logger = logging.getLogger(__name__)


class HTTPFetchError(RuntimeError):
    pass


class HTTPFetcher:
    def __init__(self, loop, session, max_tries, delay):
        self.loop = loop
        self.session = session
        self.max_tries = max_tries
        self.delay = delay

    async def __call__(self, url, request):
        der_request = request.public_bytes(Encoding.DER)
        headers = {'Content-Type': 'application/ocsp-request'}

        @async_retry(max_tries=self.max_tries, delay=self.delay,
                     loop=self.loop)
        async def send():
            resp = await self.session.post(url, data=der_request,
                                           headers=headers)
            resp.raise_for_status()
            if resp.content_type != 'application/ocsp-response':
                raise HTTPFetchError("Wrong content type: %r" %
                                     resp.content_type)
            return await resp.read()

        body = await send()
        return ocsp.load_der_ocsp_response(body)
