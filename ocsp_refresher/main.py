import argparse
import asyncio
import logging

import aiohttp

from . import config
from .httpfetcher import HTTPFetcher
from .refresher import OCSPRefresher


logger = logging.getLogger(__name__)


def _make_argparser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--config',
                        type=str,
                        default='/etc/ocsp_refresher/config.yaml',
                        help='main configuration file')
    parser.add_argument('--dry-run',
                        action='store_true',
                        help=('stop before actually'
                              ' sending out the OCSP requests'))
    return parser


def _setup_logging():
    root_logger_name = '.'.join(__name__.split('.')[:-1])
    root_logger = logging.getLogger(root_logger_name)

    root_logger.setLevel(config.get('logging_level'))

    fmt_template = '%(asctime)s %(name)s %(levelname)s: %(message)s'
    formatter = logging.Formatter(fmt_template)
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)

    root_logger.addHandler(handler)


def main(cmd_args=None):
    parser = _make_argparser()
    args = parser.parse_args(cmd_args)

    config.read_config(args.config)
    _setup_logging()

    loop = asyncio.get_event_loop()
    http_session = aiohttp.ClientSession(loop=loop)

    fetcher = HTTPFetcher(
        delay=config.get('http_retry_delay'),
        loop=loop,
        max_tries=config.get('http_retry'),
        session=http_session,
    )
    # verifier =
    refresher = OCSPRefresher(
        fetcher=fetcher,
        loop=loop,
        max_residual_life=config.get('max_residual_life'),
        # verifier=verifier,
    )

    # ph_handlers = config.get('post_update_handlers')
    # for h in ph_handlers:
    #     refresher.add_handler()

    certificates = config.get('certificates')
    cert_tasks = []
    for c in certificates:
        cert_path = c['cert_path']
        issuer_path = c['issuer_path']
        ocsp_path = c['ocsp_path']
        t = loop.create_task(refresher(cert_path, issuer_path, ocsp_path))
        cert_tasks.append(t)
    all_results = asyncio.gather(*cert_tasks, loop=loop,
                                 return_exceptions=True)
    loop.run_until_complete(all_results)
    for r in all_results.result():
        if isinstance(r, Exception):
            logger.error("Refresh of certificate failed", exc_info=r)

    close_http_session_task = loop.create_task(http_session.close())
    loop.run_until_complete(close_http_session_task)
    loop.close()
