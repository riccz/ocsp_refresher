import itertools
import logging
import pathlib

import yaml


logger = logging.getLogger(__name__)
_config = None
_default_config = {
    'certificates': [],
    'http_retry': 3,
    'http_retry_delay': 60,
    'logging_level': 'WARNING',
    'max_residual_life': 0.33,
    'post_update_handlers': [],
}


class ConfigError(RuntimeError):
    pass


def _read_section(sec, root, data):
    subdir = root.joinpath(sec)
    if not subdir.exists():
        return
    logger.debug("Reading section %r configuration from directory %s",
                 sec, subdir)

    subdir = subdir.resolve()
    if not subdir.is_dir():
        raise ConfigError("{!s} is not a directory".format(subdir))
    sec_items = []
    for fp in itertools.chain(subdir.glob('*.yaml'), subdir.glob('*.yml')):
        logger.debug("Reading part of section %r configuration from file %s",
                     sec, fp)
        try:
            with fp.open('r') as f:
                sec_item = yaml.safe_load(f)
        except (OSError, yaml.YAMLError) as exc:
            raise ConfigError("Cannot read config subfile at {!s}"
                              .format(fp)) from exc
        sec_items.extend(sec_item)

    if sec in data:
        raise ConfigError("Section {!r} is defined twice".format(sec))
    data[sec] = sec_items


def read_config(path):
    logger.info("Reading main configuration from file %s", path)
    path = pathlib.Path(path)
    if not path.exists():
        raise ConfigError("Config file {!s} does not exist".format(path))
    path = path.resolve()

    try:
        with path.open('r') as f:
            data = yaml.safe_load(f)
    except (OSError, yaml.YAMLError) as exc:
        raise ConfigError("Cannot read config at {!s}".format(path)) from exc

    sections = ['certificates', 'post_update_handlers']
    for sec in sections:
        _read_section(sec, path.parent, data)

    global _config
    if _config is not None:
        logger.warning("Overwriting existing configuration")
    _config = _default_config.copy()
    _config.update(data)
    return _config


def get(key):
    return _config[key]
