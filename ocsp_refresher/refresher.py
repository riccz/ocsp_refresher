from datetime import datetime
import asyncio
import logging
import pathlib

from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.serialization import Encoding
from cryptography.x509 import ocsp
from cryptography.x509 import oid
from cryptography.x509.ocsp import OCSPResponseStatus


logger = logging.getLogger(__name__)


class OCSPRefresher:
    def __init__(self, max_residual_life, fetcher,
                 verifier=None, loop=None):
        if verifier is None:
            verifier = _disabled_verifier
        if loop is None:
            loop = asyncio.get_event_loop()
        assert max_residual_life >= 0 and max_residual_life <= 1

        self.max_residual_life = max_residual_life
        self.fetcher = fetcher
        self.verifier = verifier
        self.loop = loop
        self.handlers = []

    def add_handler(self, handler):
        self.handlers.append(handler)

    async def refresh(self, cert, issuer):
        ocsp_req = self._make_request(cert, issuer)
        ocsp_resp = None
        for url in extract_ocsp_urls(cert):
            try:
                ocsp_resp = await self.fetcher(url, ocsp_req)
            except RuntimeError as exc:
                logger.error("Fetching response from %r failed",
                             url, exc_info=exc)
                ocsp_resp = None
                continue

            if ocsp_resp.response_status != OCSPResponseStatus.SUCCESSFUL:
                logger.error("OCSP response from %r is not successful: %r",
                             url, ocsp_resp.response_status)
                ocsp_resp = None
                continue

            logger.debug("Successful OCSP response from %r", url)
            break

        if ocsp_resp is None:
            raise OCSPRefresherError("Could not fetch an OCSP response")

        valid = await self.verifier(response=ocsp_resp, request=ocsp_req,
                                    cert=cert, issuer=issuer)
        if not valid:
            raise OCSPRefresherError("OCSP response is not valid")

        return ocsp_resp

    async def __call__(self, cert_path, issuer_path, ocsp_path):
        ocsp_path = pathlib.Path(ocsp_path)
        if ocsp_path.exists():
            ocsp_path = ocsp_path.resolve()
            with ocsp_path.open('rb') as f:
                last_ocsp = ocsp.load_der_ocsp_response(f.read())
            if not self._should_refresh(last_ocsp):
                logger.debug("OCSP response at %s does not need a refresh",
                             ocsp_path)
                return

        with pathlib.Path(cert_path).open('rb') as f:
            cert = x509.load_pem_x509_certificate(f.read(),
                                                  default_backend())
        with pathlib.Path(issuer_path).open('rb') as f:
            issuer = x509.load_pem_x509_certificate(f.read(),
                                                    default_backend())
        ocsp_resp = await self.refresh(cert, issuer)

        with ocsp_path.open('wb') as f:
            f.write(ocsp_resp.public_bytes(Encoding.DER))
        logger.info("Written OCSP response to %s", ocsp_path)

        await self._call_handlers(cert_path=cert_path,
                                  issuer_path=issuer_path,
                                  ocsp_path=ocsp_path,
                                  cert=cert,
                                  issuer=issuer,
                                  ocsp_resp=ocsp_resp)

    async def _call_handlers(self, cert_path, issuer_path, ocsp_path,
                             cert, issuer, ocsp_resp):
        pass  # FIXME

    def _should_refresh(self, last_ocsp):
        # FIXME: next_update is OPTIONAL
        total_lifetime = last_ocsp.next_update - last_ocsp.this_update
        residual = last_ocsp.next_update - datetime.now()
        return residual <= total_lifetime * self.max_residual_life

    def _make_request(self, cert, issuer):
        builder = ocsp.OCSPRequestBuilder()
        builder = builder.add_certificate(cert, issuer, hashes.SHA1())
        return builder.build()


def extract_ocsp_urls(cert):
    aia_oid = oid.ExtensionOID.AUTHORITY_INFORMATION_ACCESS
    try:
        aia_ext = cert.extensions.get_extension_for_oid(aia_oid)
    except x509.ExtensionNotFound as exc:
        raise OCSPRefresherError("Authority Information Access"
                                 " extension not found") from exc

    ocsp_oid = oid.AuthorityInformationAccessOID.OCSP
    ocsp_only = filter(lambda ad: ad.access_method == ocsp_oid,
                       aia_ext.value)
    ocsp_urls = [ad.access_location.value for ad in ocsp_only]

    if len(ocsp_urls) == 0:
        raise OCSPRefresherError("No OCSP access method found")
    else:
        return ocsp_urls


async def _disabled_verifier(*args, **kwargs):
    logger.warning("OCSP response verification is DISABLED")
    return True


class OCSPRefresherError(RuntimeError):
    pass
