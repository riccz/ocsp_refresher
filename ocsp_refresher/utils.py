import asyncio
import functools
import logging


logger = logging.getLogger(__name__)


def async_retry(max_tries, delay, exceptions=(RuntimeError,), loop=None):
    def wrapper(func):
        @functools.wraps(func)
        async def wrapped(*args, **kwargs):
            i = 0
            while True:
                try:
                    result = await func(*args, **kwargs)
                except exceptions as exc:
                    i += 1
                    if i < max_tries:
                        logger.warning("Call to %s failed %d/%d times."
                                       " Retrying in %f secs",
                                       func.__name__,
                                       i, max_tries,
                                       delay,
                                       exc_info=exc)
                        await asyncio.sleep(delay, loop=loop)
                    else:
                        raise exc
                else:
                    return result
        return wrapped
    return wrapper
