#!/usr/bin/env python

import pathlib

from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.serialization import Encoding
import psycopg2

_here = pathlib.Path(__file__).parent


def get_many_uris(conn):
    with conn.cursor() as c:
        c.execute('''WITH c AS (
                         SELECT * FROM certificate
                         WHERE now() BETWEEN x509_notBefore(certificate) AND
                                             x509_notAfter(certificate)
                     ) SELECT certificate FROM c WHERE 1 < (
                           SELECT count(*)
                           FROM x509_authorityInfoAccess(c.certificate, 1)
                     ) LIMIT 1''')
        cert = c.fetchall()[0][0]
    return x509.load_der_x509_certificate(cert, default_backend())


def get_n_uris(conn, n):
    with conn.cursor() as c:
        c.execute('''WITH c AS (
                         SELECT * FROM certificate
                         WHERE now() BETWEEN x509_notBefore(certificate) AND
                                             x509_notAfter(certificate)
                     ) SELECT certificate FROM c WHERE %s = (
                           SELECT count(*)
                           FROM x509_authorityInfoAccess(c.certificate, 1)
                     ) LIMIT 1''', (n,))
        cert = c.fetchall()[0][0]
    return x509.load_der_x509_certificate(cert, default_backend())


def main():
    sslrootcert = str(_here / 'crtsh_fullchain.pem')
    conn = psycopg2.connect(
        host='crt.sh', port=5432, dbname='certwatch',
        user='guest',
        sslmode='verify-full', sslrootcert=sslrootcert
    )
    conn.autocommit = True

    no_ocsp = get_n_uris(conn, 0)
    one_ocsp = get_n_uris(conn, 1)
    more_ocsp = get_many_uris(conn)

    conn.close()

    with (_here / '../tests/data/no_ocsp.pem').open('wb') as f:
        f.write(no_ocsp.public_bytes(Encoding.PEM))
    with (_here / '../tests/data/one_ocsp.pem').open('wb') as f:
        f.write(one_ocsp.public_bytes(Encoding.PEM))
    with (_here / '../tests/data/more_ocsp.pem').open('wb') as f:
        f.write(more_ocsp.public_bytes(Encoding.PEM))


if __name__ == '__main__':
    main()
