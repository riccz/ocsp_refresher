#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = [
    'aiohttp',
    'cryptography',
    'pyyaml',
]

extras_requirements = {
    'example_data': ['psycopg2']
}

setup_requirements = [
    'pytest-runner',
]

test_requirements = [
    'pytest',
]

setup(
    author="riccz",
    author_email='ricc@zanl.eu',
    classifiers=[],
    description="OCSP refresher",
    entry_points={
        'console_scripts': [
            'ocsp_refresher=ocsp_refresher.main:main',
        ],
    },
    install_requires=requirements,
    license="",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='',
    name='ocsp_refresher',
    python_requires='>=3.5',
    packages=find_packages(include=['ocsp_refresher'], exclude=['tests']),
    setup_requires=setup_requirements,
    extras_require=extras_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    # url='https://github.com/riccz/ocsp_refresher',
    version='0.1',
    zip_safe=False,
)
