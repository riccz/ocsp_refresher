import pathlib

import pytest

from cryptography import x509
from cryptography.hazmat.backends import default_backend

from ocsp_refresher.refresher import extract_ocsp_urls, OCSPRefresherError


_here = pathlib.Path(__file__).parent


@pytest.fixture
def no_ocsp():
    with (_here / 'data/no_ocsp.pem').open('rb') as f:
        return x509.load_pem_x509_certificate(f.read(), default_backend())


@pytest.fixture
def one_ocsp():
    with (_here / 'data/one_ocsp.pem').open('rb') as f:
        return x509.load_pem_x509_certificate(f.read(), default_backend())


@pytest.fixture
def more_ocsp():
    with (_here / 'data/more_ocsp.pem').open('rb') as f:
        return x509.load_pem_x509_certificate(f.read(), default_backend())


def test_single_url(one_ocsp):
    urls = extract_ocsp_urls(one_ocsp)
    assert len(urls) == 1
    # The URL changes if `utils/download_example_certificates.py` is run again
    assert list(urls) == ['http://ocsp.int-x3.letsencrypt.org']


def test_no_url(no_ocsp):
    with pytest.raises(OCSPRefresherError):
        _ = extract_ocsp_urls(no_ocsp)


def test_many_urls(more_ocsp):
    urls = extract_ocsp_urls(more_ocsp)
    assert len(urls) == 3
    # The URLs change if `utils/download_example_certificates.py` is run again
    assert sorted(urls) == ['http://ocsp1.netlock.hu/olsslgca.cgi',
                            'http://ocsp2.netlock.hu/olsslgca.cgi',
                            'http://ocsp3.netlock.hu/olsslgca.cgi']
